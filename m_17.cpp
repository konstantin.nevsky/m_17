// m_17.cpp
//

#include <iostream>

class Vector
{

private:
    double x = 0;
    double y = 0;
    double z = 0;

public:
    Vector()
    {}
    Vector(double x, double y, double z)
    {
        this->x = x;
        this->y = y;
        this->z = z;
    }
    void SetX(double x)
    {
        this->x = x;
    }
    void SetY(double y)
    {
        this->y = y;
    }
    void SetZ(double z)
    {
        this->z = z;
    }
    double GetX()
    {
        return this->x;
    }
    double GetY()
    {
        return this->y;
    }
    double GetZ()
    {
        return this->z;
    }
    double Length()
    {
        return sqrt(this->x * this->x + this->y * this->y + this->z * this->z);
    }
};

int main()
{

    Vector V1;
    std::cout << "x: " << V1.GetX() << ", y: " << V1.GetY() << ", z: " << V1.GetZ() << ", Length: " << V1.Length() << "\n";
    Vector V2(1, 2, 3);
    std::cout << "x: " << V2.GetX() << ", y: " << V2.GetY() << ", z: " << V2.GetZ() << ", Length: " << V2.Length() << "\n";
    Vector V3(12, 0, 0);
    std::cout << "x: " << V3.GetX() << ", y: " << V3.GetY() << ", z: " << V3.GetZ() << ", Length: " << V3.Length() << "\n";

}